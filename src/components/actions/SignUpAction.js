import React from 'react';
import { useSelector, useDispatch, Provider } from 'react-redux';
import { API } from '../../Utils/BaseUrl';
import { Alert } from 'react-native';
import showToast from '../../Utils/ShowToast';

//----- Register Api
// Params { first_name, last_name, email, password, password_confirmation, profile }
export const SignupAction = (params, navigation) => {
    var formData = new FormData();
    formData.append('first_name', params.first_name);
    formData.append('last_name', params.last_name);
    formData.append('email', params.email);
    formData.append('password', params.password);
    formData.append('password_confirmation', params.password_confirmation);
    formData.append('profile', params.profile);

    return async dispatch => {
        fetch(`${API.BASE}/api/auth/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: formData,
        })
            .then(res => res.json())
            .then(res => {
                if (res.status === true) {
                    Alert.alert(res.message)
                    dispatch({ type: 'Register', payload: res });
                } else if (res.status != true) {
                    Alert.alert(res.message)
                    dispatch({ type: 'Register', payload: res });
                }
            })
            .catch(e => {
                if (e.message === 'Network request failed') {
                    Alert.alert('No Internet Connection');
                }
            });
    };
};
