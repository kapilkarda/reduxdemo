import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    Image,
    TouchableOpacity,
    TextInput,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-crop-picker';
import { Images } from '../Utils/Images';
import { ImageUploadApi } from '../components/actions/ImageUploadAction';
import { useDispatch, useSelector } from 'react-redux';
import { SignupAction } from '../components/actions/SignUpAction';

const ResponsiveHeight = Dimensions.get('window').height;
const ResponsiveWidth = Dimensions.get('window').width;

export const SignUpScreen = ({ navigation }) => {

    const dispatch = useDispatch();

    const GetUploadResponse = useSelector(state => state.UploadImageReducer.IMAGEUPLOADRES);
    const SignUpSuccessRes = useSelector(state => state.SignUpReducer.SIGNUPRES);
    console.log("SignUpSuccessRes",SignUpSuccessRes);

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [cnfpassword, setCnfPassword] = useState('');
    const [profileImage, setImageProfile] = useState('');

    //----- This useeffect call when the image get uploaded -----//
    useEffect(() => {
        if (GetUploadResponse && GetUploadResponse.status === true) {
            setImageProfile(GetUploadResponse.url)
        }
    }, [GetUploadResponse]);

     //----- This useeffect call when the user get successfully Register -----//
     useEffect(() => {
        if (SignUpSuccessRes && SignUpSuccessRes.status === true) {
            dispatch({ type: 'UPLOADIMAGE', payload: '' });
            navigation.navigate('CatListScreen');
        }
    }, [SignUpSuccessRes]);

    // ------ Image Upload from Gallery Function ------ //
    const openLibraryFunc = () => {
        ImagePicker.openPicker({
            cropping: false,
            width: 500,
            height: 500,
            cropperCircleOverlay: true,
            compressImageMaxWidth: 640,
            compressImageMaxHeight: 480,
            freeStyleCropEnabled: true,
            includeBase64: true,
        })
            .then(response => {
                const fileName = response.path.split('/');
                const sendData = {
                    path: response.path,
                    name: fileName ? fileName[fileName.length - 1] : '',
                    type: response.mime,
                };
                dispatch(ImageUploadApi(sendData));
            })
            .catch(err => {
                console.log('openLibraryFunc err', err);
            });
    };

    const signUpApiCall = () => {
        const sendData = {
            first_name: firstName,
            last_name: lastName,
            email: email,
            password: password,
            password_confirmation: cnfpassword,
            profile: profileImage
        }
        console.log("sendData", sendData);
        dispatch(SignupAction(sendData))
    }

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={styles.inputMainView}>
                    <Text style={styles.inputTitleStyle}>First Name</Text>
                    <TextInput
                        placeholder="Enter your first name"
                        placeholderTextColor={'grey'}
                        style={styles.textInputStyle}
                        onChangeText={(text) => setFirstName(text)}
                    />
                </View>

                <View style={styles.inputMainView}>
                    <Text style={styles.inputTitleStyle}>Last Name</Text>
                    <TextInput
                        placeholder="Enter your last name"
                        placeholderTextColor={'grey'}
                        style={styles.textInputStyle}
                        onChangeText={(text) => setLastName(text)}
                    />
                </View>

                <View style={styles.inputMainView}>
                    <Text style={styles.inputTitleStyle}>Email</Text>
                    <TextInput
                        placeholder="Enter your email"
                        placeholderTextColor={'grey'}
                        style={styles.textInputStyle}
                        onChangeText={(text) => setEmail(text)}
                    />
                </View>

                <View style={styles.inputMainView}>
                    <Text style={styles.inputTitleStyle}>Password</Text>
                    <TextInput
                        placeholder="Enter password"
                        placeholderTextColor={'grey'}
                        style={styles.textInputStyle}
                        onChangeText={(text) => setPassword(text)}
                    />
                </View>

                <View style={styles.inputMainView}>
                    <Text style={styles.inputTitleStyle}>Confirm Password</Text>
                    <TextInput
                        placeholder="Confirm password"
                        placeholderTextColor={'grey'}
                        style={styles.textInputStyle}
                        onChangeText={(text) => setCnfPassword(text)}
                    />
                </View>
                <View style={styles.uploadProfileMainView}>
                    <Text style={styles.inputTitleStyle}>Upload Profile</Text>
                    <TouchableOpacity
                        onPress={() => openLibraryFunc()}
                        style={{
                            alignSelf: 'center'
                        }}>
                        {profileImage != '' ? (
                            <Image source={{ uri: profileImage }} style={styles.dummyImageStyle} />
                        ) : (
                            <Image source={Images.dummyImage} style={styles.dummyImageStyle} />
                        )}

                    </TouchableOpacity>
                </View>

                <TouchableOpacity onPress={() => signUpApiCall()} style={styles.signUpBtnStyle}>
                    <Text style={{ color: '#fff', fontSize: 16, fontWeight: 'bold' }}>Sign Up</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
};

export default SignUpScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    inputMainView: {
        alignSelf: 'center',
        marginTop: 10,
    },
    inputTitleStyle: {
        fontSize: 16,
        fontWeight: '600',
    },
    textInputStyle: {
        height: ResponsiveHeight / 15,
        width: ResponsiveWidth - 20,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 5,
        fontSize: 16,
    },
    uploadProfileMainView: {
        marginTop: 10,
        paddingHorizontal: 12
    },
    dummyImageStyle: {
        height: ResponsiveHeight / 4,
        width: ResponsiveWidth / 4,
        resizeMode: 'contain',
        borderRadius: ResponsiveHeight / 2
    },
    signUpBtnStyle: {
        height: 50,
        width: ResponsiveWidth - 50,
        backgroundColor: '#6448DC',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        alignSelf: 'center',
        marginVertical: 20
    }
});
